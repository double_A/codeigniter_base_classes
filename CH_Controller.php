<?php
class CH_Controller extends CI_Controller 
{

    //:TODO made config file to declare there main configs such as folder with default views
    //
    /** Template page view
     *
     * Render function will use this var to load
     * template page view
     *
     * @var string $page_template
     */
    var $page_template = null;

    /** Default page template
     *
     * @var string
     */
    var $def_page_template = 'light/template';

    /** Array of component templates
     *
     *  Render function will use this array to load
     *  component views
     *
     * @var array $page_component_templates
     */
    var $page_component_templates = array();

    /** Array of component data
     *
     * Render function will use this array as data source
     * for component views
     *
     * @var array $page_component_data
     */
    var $page_component_data = array();

    /** Array of defaul templates
     *
     * Render function will use this array
     * as default settings
     *
     * @var array $def_page_component_templates
     */
    var $def_page_component_templates = array(
        'head' => 'light/core/head',
        'header' => 'light/core/header',
        'footer' => 'light/core/footer',
    );

    /**
     * @var array $def_page_component_data
     */
    var $def_page_component_data = array();

    /** Array of prerendered parts of template
     *
     * @var array $page
     */
    protected $page = array();
    
    //------------------------------------------------------------------------//

    /**
     *  True to enable autorendering
     *
     * @var bool $isAutorendering
     */
    //:TODO fix autorender
    var $isAutorendering = false;
    
    //------------------------------------------------------------------------//
    
    var $userData = array();

    //------------------------------------------------------------------------//

    var $SEO = null;

    //------------------------------------------------------------------------//
    function __construct() 
    {
        parent::__construct();


        $this->_initWithDefault();
        // :TODO до лучших времен...
        $this->user_data = null;
    }

    /**
     *
     */
    protected function _renderPage() 
    {
      // $this->page_component_data['menu']['user_data'] = $this->user_data;

        //:debug
        //var_dump($this->page_component_templates);
        //var_dump($this->page_component_data);

        foreach($this->page_component_templates as $component_name => $component_template)
        {
            $this->page[$component_name] = $this->load->view(
                    $component_template,
                    $this->page_component_data[$component_name],
                    true);
        }

        // load mane page template
        $this->load->view($this->page_template, $this->page);
    }
    
    //------------------------------------------------------------------------//

    /**
     *  Redeclare already initialized view for component
     *  Can be used to redaclere default settings
     *
     * @param $page_component
     * @param $view
     */
    protected function _rediclareView($page_component, $view)
    {
        $this->page_component_templates[$page_component] = $view;
    }

    /** Переопределяет основной шаблон страницы
     *
     * @param $template
     */
    protected function _rediclarePageTemplate($template)
    {
        $this->page_template = $template;
    }

    //------------------------------------------------------------------------//

    /** Set data for all of the components
     *
     *
     * @param $data_array
     */
    protected function _setPageData($data_array)
    {
        $this->page_component_data = $data_array;
    }

    /** Set or redeclare data for page component
     *
     * @param $component_name
     * @param array $data
     */
    protected function _setPageComponentData($component_name, $data = null)
    {
        $this->page_component_data[$component_name] = $data;
    }


    /** Append data ($data) to array of page component data
     * with name $componemtName
     * and index $arrayIndexName
     *
     * @param $componemtName
     * @param $arrayIndexName
     * @param $data
     */
    protected function _appendPageComponentData($componemtName, $arrayIndexName, $data = null )
    {
        $this->page_component_data[$componemtName][$arrayIndexName] = $data;
    }

    /** Append data ($data) to array of DEFAULT page component data
     * with name $componemtName
     * and index $arrayIndexName
     *
     * Use this function in child object
     * (if array of default data defined in child object)
     *
     * @param $componemtName
     * @param $arrayIndexName
     * @param $data
     */
    protected function _appendPageComponentDataAsDefault($componemtName, $arrayIndexName, $data = null )
    {
        $this->def_page_component_data[$componemtName][$arrayIndexName] = $data;
    }

    /** Собирает из массива данных $data(массив массивов) и названия партиала $partialName
     *  массив отрендереных партиалов и передаёт в качестве данных
     *  в комопонент страницы с названием $componentName
     *
     * @param string $componentName
     * @param string $partialName
     * @param array $data
     * @param string $variable_name
     */
    protected function _setPageComponentDataWithArrayOfPartials ($componentName, $partialName, $data = array(), $variable_name = 'data') // O_o
    {
        $componentData = null;

        foreach($data as $value)
            $componentData[$variable_name][] = $this->_partial($partialName, $value);

        $this->_setPageComponentData($componentName, $componentData);
    }

    //------------------------------------------------------------------------//
    
    /*
     *  Добавляет компонент, вид и данные в список на рендеринг
     */

    protected function _addPageComponent($component , $component_template = null, $component_data = null)
    {
        if($component_template == null)
            $component_template = 'default/' . $component_template;
        
        $this->page_component_templates[$component] = $component_template;
        $this->page_component_data[$component] = $component_data;
        
    }

    /** Remove component from render list
     *
     * @param $component
     */
    protected function _removePageComponent($component)
    {
        unset($this->page_component_templates[$component]);
    }
    
    /*
     * 
     */
    protected function _setPageComponents($components)
    {
        $this->page_component_templates = $components;
    }


    //------------------------- Helpers --------------------------------------//

    /** Return rendered HTML code from $view with $data
     * Возвращает
     *
     * @param string $view
     * @param array $data
     * @return string
     */
    protected function _partial($view, $data)
    {
        return $this->load->view($view, $data, true);
    }

    /** Вызывается в дочернем классе
     * (либо в конструкторе CH_Controller , если
     * все параметры по умолчанию определены в CH_Controller)
     *
     * Инициализирует параметры шаблонизаторв значениями по умолчанию
     * в том числе и SEO данные
     */
    protected function _initWithDefault()
    {
        //getting seo
//        $this->SEO = $this->seomodel->get($this->uri->uri_string());
//        $this->_appendPageComponentDataAsDefault('head', 'seo', array(
//            'title' => $this->SEO['title'],
//            'keywords' => $this->SEO['keywords'],
//            'description' => $this->SEO['descr']
//        ));

        //setting up default template

//        if($this->tank_auth->is_logged_in())
//        {
            $this->_appendPageComponentData('header','user', array('email' => 'email'));
//        }

        $this->page_template = $this->def_page_template;

        // setting up default configs
        $this->page_component_templates = $this->def_page_component_templates;

        // initializing data array for all components
        foreach($this->page_component_templates as $key => $value)
            $this->page_component_data[$key] = null;

        //забиваем все дефолтные данные в компоненты, если они объявленны
        foreach($this->def_page_component_data as $key => $value)
            $this->page_component_data[$key] = $value;
    }

}
?>

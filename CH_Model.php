<?php
/**
 * Created at 12.04.13 03:37
 *
 * @author chapai
 * @category Base_classes Factory
 * @package Factory
 * @version 1.0
 * @copyright doublea
 */

class CH_Model extends CI_Model
{

    /**
     * @var string
     */
    var $_tablename = '';

    function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null $whereArray
     *
     * @return mixed
     */
    function get($whereArray = null, $limit = null)
    {
        $this->db
            ->select()
            ->from($this->_tablename);

        if(!is_null($whereArray))
            $this->db->where($whereArray);

        if(!is_null($limit))
            $this->db->limit($limit);

        return $this->db->get()->result();

    }

    /**
     * @param array $entity
     *
     * @return mixed
     */
    function post($entity)
    {
        $this->db->insert($this->_tablename,$entity);

        return $this->db->insert_id();
    }

    /**
     * @param array $updateArray
     * @param array $whereArray
     *
     * @return mixed
     */
    function patch($updateArray, $whereArray)
    {
        $this->db->where($whereArray);
        return $this->db->update($this->_tablename, $updateArray);
    }

    /**
     * @param array $whereArray
     *
     * @return mixed
     */
    function delete($whereArray)
    {
        return $this->db->delete($this->_tablename, $whereArray);
    }
}